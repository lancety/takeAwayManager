export class Content {
    regDate = {type: Date, default: Date.now};
    updateDate = {type: Date, default: Date.now};
}

export const Geocoder = {
    results: {
        address_comments: {},
        formatted_address: String,
            geometry: {
            bounds: {},
            location: {},
            location_type: String,
                viewport: {}
        },
        place_id: String,
            types: [String]
    },
    status: String
}