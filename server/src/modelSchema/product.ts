var clone = require("clone");

import {M, Schema, ObjectId, Document} from "../rest/dbConn";
import {IProduct} from "../model/product";
import {Content} from "./generic";

export interface ProductMD extends IProduct, Document {
}

const Choice = {
    name: {type: String, required: true, index: true},
    amountDefault: {type: Number, index: true},
    priceDefault: {type: Number, index: true},
    comment: String
};


class Product_base extends Content {
    name = {type: String, index: true, required: true};
    comment = String;
    category = {type: String, index: true};
    price = {type: Number, index: true, required: true};
    choice = {type: Choice, index: false, required: false};
    regDate = {type: Date, default: Date.now};
    updateDate = {type: Date, default: Date.now};
}

const Product_base_instance = new Product_base();

export const ProductSchema = new Schema(Product_base_instance, {collection: "product"});

export const ProductModel = M.model<ProductMD>("Product", ProductSchema);



const Order_choice = clone(Choice);
Order_choice.amount = {type: Number, index: true};
Order_choice.price = {type: Number, index: true};

class Order_product extends Product_base {
    amount = Number;
    priceTotal = Number;
    choice = Order_choice;
}

export const OrderProductSchema = new Schema(new Order_product(), {collection: "orderProduct"});

export const OrderProductModel = M.model<ProductMD>("OrderProduct", OrderProductSchema);