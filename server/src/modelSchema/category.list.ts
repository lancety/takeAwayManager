import {M, Schema, Document} from "../rest/dbConn";
import {ICategoryList} from "../model/generic";

export interface CategoryListMD extends ICategoryList, Document {
}


function getSchema(collectionName, additionalProperty) {
    var schema = new Schema({
        owner_id: String,
        categories: [{
            name: String,
            items: [{
                id: String,
                name: String
            }]
        }],
        updateDate: {type: Date, default: Date.now},
        regDate: {type: Date, default: Date.now}
    }, {collection: collectionName});

    if (additionalProperty) {
        schema.add(additionalProperty);
    }

    return schema;
}
class CategoryList {
    owner_id = String;
    categories = [{
        name: String,
        items: [{
            id: String,
            name: String
        }]
    }];
    updateDate = {type: Date, default: Date.now};
    regDate = {type: Date, default: Date.now};
}

const CategoryList_instance = new CategoryList();

export const CategoryListSchema_Product = new Schema(CategoryList_instance, {collection: "categoryList_product"});
export const CategoryListModel_Product = M.model<CategoryListMD>(
    "ProductCategoryList",
    CategoryListSchema_Product);


export const CategoryListSchema_Client = new Schema(CategoryList_instance, {collection: "categoryList_client"});
export const CategoryListModel_Client = M.model<CategoryListMD>(
    "ClientCategoryList",
    CategoryListSchema_Client);