import {M, Schema, ObjectId, Document} from "../rest/dbConn";
import {IOrder, OrderType, OrderMeal, OrderStatus} from "../model/order";

export const OrderSchema = new Schema({
    orderNumber: {type: String, required: true, index: true},
    client: { type: ObjectId, ref: "OrderClient", required: true, index: true},
    product: { type: [ObjectId], ref: "OrderProduct", required: true, index: true},
    type: {type: String, index: true},
    meal: {type: String, index: true},
    date: {type:Date, index: true},
    status: {type: String, index: true},
    priceTotal: {type: Number, default: 0},
    payment: {type: Number, default: 0},
    regDate: {type: Date, default: Date.now},
    updateDate: {type: Date, default: Date.now}
}, {collection: "order"});

export interface OrderMD extends IOrder, Document {}

export const Order = M.model<OrderMD>("Order", OrderSchema);