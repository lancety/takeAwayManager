import {IClient} from "../model/client";
import {M, Schema, Document} from "../rest/dbConn";
import {Content, Geocoder} from "./generic";

export interface ClientMD extends IClient, Document {}


class Client_base extends Content {
    account = {type: String, index: true};
    name = {type: String, index: true, required: true};
    mobile = {type: String, index: true};
    address = {type: Geocoder, index: true};
    orderSummary = {
        done: {type: Number, default: 0},
        pass: {type: Number, default: 0},
        cancel: {type: Number, default: 0}
    };
}

const Client_base_instance = new Client_base();

export const ClientSchema = new Schema( Client_base_instance, {collection: "client"});

export const ClientModel = M.model<ClientMD>("Client", ClientSchema);



class Order_client extends Client_base {}

const Order_client_instance = new Order_client();

export const OrderClientSchema = new Schema( Order_client_instance, {collection: "orderClient"});

export const OrderClientModel = M.model<ClientMD>("OrderClient", OrderClientSchema);