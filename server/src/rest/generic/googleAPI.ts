var NodeGeocoder = require("node-geocoder");

var geocoder = NodeGeocoder("google", "http");

export function validateAddress(address: string){
    return geocoder.geocode(address).then(
        function(res){
            return res;
        }, function(err){
            return err;
        }
    );
}

export function validateAddressSimple(address){
    return validateAddress(address).then(
        function(res){
            let addressOptions = [];
            for (let address of res){
                addressOptions.push(address.formattedAddress);
            }

            return addressOptions;
        }
    )
}