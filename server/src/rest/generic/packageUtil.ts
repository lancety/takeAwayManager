import {queryStatus} from "../../model/generic";
import {Model} from "mongoose";

export function getContent(contentModel: Model<any>, contentId:string) {
    return contentModel.findById(contentId).exec().then(
        function (model) {
            return {
                status: queryStatus.done, feedback: {affected: 1, affectedItems: [model["_doc"]]}
            };
        }, function (err) {
            return {
                status: queryStatus.fail, err: err
            }
        }
    );
}

export function createContent(contentModel: Model<any>, content: any) {
    delete content["_id"];

    content.regDate = (new Date()).toISOString();
    content.updateDate = (new Date()).toISOString();

    return contentModel.create(content).then(
        function (model) {
            return {
                status: queryStatus.done, feedback: {affected: 1, affectedItems: [model["_doc"]]}
            };
        }, function (err) {
            return {
                status: queryStatus.fail, err: err
            }
        }
    );
}

export function updateContent(contentModel: Model<any>, content: any) {
    let id = content["_id"];

    delete content["_id"];
    delete content.regDate;
    content.updateDate = (new Date()).toISOString();

    return contentModel.findOneAndUpdate({"_id": id}, content).exec().then(
        function (model) {
            return {
                status: queryStatus.done, feedback: {affected: 1, affectedItems: [model["_doc"]]}
            };
        }, function (err) {
            return {
                status: queryStatus.fail, err: err
            }
        }
    );
}

export function deleteContent(contentModel: Model<any>, content: any) {
    let id = content["_id"];

    return contentModel.findOneAndRemove({"_id": id}).exec().then(
        function (model) {
            return {
                status: queryStatus.done, feedback: {affected: 1, affectedItems: [model["_doc"]]}
            };
        }, function (err) {
            return {
                status: queryStatus.fail, err: err
            }
        }
    );
}