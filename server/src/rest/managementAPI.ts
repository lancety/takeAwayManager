import * as express from "express";
let router = express.Router();

import * as ClientRest from "./clientAPI";
import * as ProductRest from "./productAPI";
import * as GeoCoder from "./generic/googleAPI";

router.use("/client", ClientRest);
router.use("/product", ProductRest);

router.get("/geocoder/address/:address", (req, res) => {
    let addressString = req.params.address;

    GeoCoder.validateAddressSimple(addressString)
        .then(function(addressList){
            res.json(addressList);
        })
});

export = router;