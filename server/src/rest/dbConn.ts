///<reference path="../typings/node.d.ts" />
///<reference path="../typings/mongoose.d.ts"  />

import mongoose = require("mongoose");

export const M = mongoose.createConnection("mongodb://localhost:27017/takeAwayManager");
export class Schema extends mongoose.Schema{}
export interface Document extends mongoose.Document {}
export const ObjectId = Schema.Types.ObjectId;

