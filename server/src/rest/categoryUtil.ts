import {Model} from "mongoose";
import {ICategoryList as _ICategoryList, queryStatus} from "../model/generic";
import {CategoryListMD, CategoryListModel_Product} from "../modelSchema/category.list";

export interface ICategoryList extends _ICategoryList {
}

export function getCategoryList(_CategoryListModel:Model<CategoryListMD>) {
    return _CategoryListModel.findOne({}, {}, {sort: {"regDate": -1}}).exec().then(
        function (categoryList) {
            if (categoryList === null) {
                categoryList = new CategoryListModel_Product();
            }
            return {
                status: queryStatus.done,
                feedback: {affected: 1, affectedItems: [categoryList]}
            };
        }, function (err) {
            return {
                status: queryStatus.fail, err: err
            }
        }
    );
}

export function upsertCategoryList(_CategoryListModel:Model<CategoryListMD>,
                                   categoryList:ICategoryList) {

    let id = categoryList["_id"];

    delete categoryList._id;
    delete categoryList.updateDate;
    delete categoryList.regDate;

    if (!id) {
        // insert
        return _CategoryListModel.create(categoryList).then(
            function (categoryList) {
                return {
                    status: queryStatus.done, feedback: {affected: 1, affectedItems: [categoryList]}
                };
            }, function (err) {
                return {
                    status: queryStatus.fail, err: err
                }
            }
        );
    } else {
        // update
        categoryList.updateDate = (new Date()).toISOString();

        let options = {
            new: true,
            upsert: true
        };

        return _CategoryListModel.findOneAndUpdate(
            {"_id": id},
            categoryList,
            options ).exec().then(
            function (categoryList) {
                return {
                    status: queryStatus.done, feedback: {affected: 1, affectedItems: [categoryList]}
                };
            }, function (err) {
                return {
                    status: queryStatus.fail, err: err
                }
            }
        );
    }
}