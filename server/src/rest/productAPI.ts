import * as express from "express";

import * as PackageApi from "./generic/packageUtil";
import * as CategoryRest from "./categoryRest";

import {Product} from "../model/product";
import {ProductModel} from "../modelSchema/product";

let router = express.Router();

router.use("/category_list", CategoryRest);

router.get("/get_by_id/:id", (req, res) => {
    let id = req.params.id;
    PackageApi.getContent(ProductModel, id)
        .then(function (response) {
            res.json(response);
        })
});

router.post("/create", (req, res)=> {
    var product = req.body as Product;
    product.regDate = (new Date()).toISOString();
    product.updateDate = (new Date()).toISOString();
    PackageApi.createContent(ProductModel, product)
        .then(function (response) {
            res.json(response);
        })
});

router.put("/update", (req, res) => {
    var product = req.body as Product;
    product.updateDate = (new Date()).toISOString();
    PackageApi.updateContent(ProductModel, product)
        .then(function(response){
            res.json(response);
        })
});

router.delete("/delete", (req, res) => {
    var product = req.body as Product;
    PackageApi.deleteContent(ProductModel, product)
        .then(function(response){
            res.json(response);
        })
});

export = router;