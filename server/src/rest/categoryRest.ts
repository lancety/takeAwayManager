import * as express from "express";
import {CategoryListModel_Product, CategoryListModel_Client} from "../modelSchema/category.list";

let router = express.Router();

import * as CategoryApi from "./categoryUtil";

function getCategoryListModel(req) {
    let urlSegments = req.originalUrl.split("/");
    let type = urlSegments[urlSegments.length - 3];

    switch (type) {
        case "product":
            return CategoryListModel_Product;
        case "client":
            return CategoryListModel_Client;
    }
}


router.get("/get", function (req, res) {
    CategoryApi.getCategoryList(getCategoryListModel(req))
        .then(function (response) {
            res.json(response);
        })
});


router.post("/upsert", function (req, res) {
    CategoryApi.upsertCategoryList(
        getCategoryListModel(req),
        req.body as CategoryApi.ICategoryList).then(
        function (response) {
            res.json(response);
        }
    )
});

export = router;