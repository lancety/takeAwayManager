import * as express from "express";

import * as PackageApi from "./generic/packageUtil";
import * as CategoryRest from "./categoryRest";

import {Client} from "../model/client";
import {ClientModel, OrderClientModel} from "../modelSchema/client";

let router = express.Router();

router.use("/category_list", CategoryRest);

router.get("/get_by_id/:id", (req, res) => {
    let id = req.params.id;
    PackageApi.getContent(ClientModel, id)
        .then(function (response) {
            res.json(response);
        })
});

router.post("/create", (req, res)=> {
    var product = req.body as Client;
    PackageApi.createContent(ClientModel, product)
        .then(function (response) {
            res.json(response);
        })
});

router.put("/update", (req, res) => {
    var product = req.body as Client;
    PackageApi.updateContent(ClientModel, product)
        .then(function(response){
            res.json(response);
        })
});

router.delete("/delete", (req, res) => {
    var product = req.body as Client;
    PackageApi.deleteContent(ClientModel, product)
        .then(function(response){
            res.json(response);
        })
});

export = router;