///<reference path="./typings/node.d.ts" />
///<reference path="./typings/express.d.ts" />

import * as express from "express";
import * as bodyParser from "body-parser";

import {M} from "./rest/dbConn";
import * as ManagementRest from "./rest/managementAPI";

var app = express();

M.on("error", function (err) {
    console.log(err);
});


var serverDir:string = __dirname;
var rootDir:string = serverDir.replace(new RegExp("\/server\/sv$"), "");
var uiDir:string = rootDir + "/public";
app.use("/", express.static(uiDir));
app.use(bodyParser.json());

app.use ("/management", ManagementRest);

console.log(rootDir);
console.log(serverDir);
console.log(uiDir);

app.listen(3000, function () {
    console.log("server started on port 3000.");
});