module.exports = function (grunt) {
    grunt.initConfig({
        shell: {
            ts_public: {
                command: "tsc --project ./public/src/tsconfig.json"
            },
            tsw_public: {
                command: "tsc -w --project ./public/src/tsconfig.json"
            },
            ts_server: {
                command: "tsc --project ./server/src/tsconfig.json"
            },
            tsw_server: {
                command: "tsc -w --project ./server/src/tsconfig.json"
            },
            npm_root: {
                command: "npm install"
            },
            npm_public: {
                command: "cd public; npm install; cd ../"
            },
            npm_server: {
                command: "cd server; npm install; cd ../"
            }
        },
        concurrent: {
            ts_watch: ["shell:tsw_public", "shell:tsw_server"]
        },
        copy:{
            model_to_public: {
                //expand: true,
                //filter:"isFile",
                src:["./model/*.ts"],
                dest:"./public/src/"
            },
            model_to_server: {
                //expand: true,
                //filter:"isFile",
                src:["./model/*.ts"],
                dest:"./server/src/"
            }
        }
    });


    grunt.loadNpmTasks("grunt-shell-spawn");
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-contrib-clean");
    grunt.loadNpmTasks("grunt-concurrent");

    grunt.registerTask("npm_install", [
        "shell:npm_root",
        "shell:npm_public",
        "shell:npm_server"]);

    // copy common ts files between front end and server to their own directory
    grunt.registerTask("copy_to_public", ["copy:model_to_public"]);
    grunt.registerTask("copy_to_server", ["copy:model_to_server"]);

    // compile each end after all required ts files copied to their local folder
    grunt.registerTask("compile_public", ["shell:ts_public"]);
    grunt.registerTask("compile_server", ["shell:ts_server"]);

    grunt.registerTask("deploy_public", [
        "copy_to_public",
        "compile_public"
    ]);
    grunt.registerTask("deploy_server", [
        "copy_to_server",
        "compile_server"
    ]);

    // copy and compile
    grunt.registerTask("deploy",[
        "deploy_public",
        "deploy_server"
    ]);

    grunt.registerTask("watch", ["shell:tsw_public"]);
};
