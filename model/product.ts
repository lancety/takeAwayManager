import {IProductChoice} from "./productChoice";
import {IContent, Content} from "./generic";

export interface IProduct extends IContent {
    name:string;
    price:number;
    choice:IProductChoice[];
    comment:string;
}

export class Product extends Content implements IProduct {

    name:string;
    price:number;
    choice:IProductChoice[];
    comment:string;

    constructor(name?:string, price?:number) {
        super();

        this.name = name || "";
        this.price = price || 0;
        this.choice = [];

        this.comment = "";
    }
}

export class OrderProduct extends Product {
    amount:number;
    priceTotal:number;

    constructor(name?:string) {
        super(name);

        this.amount = 0;
        this.priceTotal = 0;
    }
}