export interface IProductChoice {
    name: string;
    priceDefault: number;
    amountDefault: number;
    comment: string;
}

export class ProductChoice implements IProductChoice{
    name:string;
    priceDefault:number;
    amountDefault:number;
    comment:string;

    constructor(name?:string, price?:number, amountDefault?:number) {
        this.name = name || "";
        this.priceDefault = price || 0;
        this.amountDefault = amountDefault || 0;
    }

    isEmpty(){
        return this.name === undefined &&
            this.priceDefault === undefined &&
            this.amountDefault === undefined;
    }
}


export class OrderProductChoice extends ProductChoice {
    amount:number;
    price:number;

    constructor(name?:string, price?:number, amountDefault?:number) {
        super(name, price, amountDefault);

        this.amount = 0;
        this.price = 0;
    }
}