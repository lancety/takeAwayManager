import {IContent, Content} from "./generic";
import {IGoogleAddressResult} from "./google.geocoder";
export interface IClient extends IContent {
    account:string;
    name:string;
    mobile:string;
    address: IGoogleAddressResult;
    orderSummary:{
        done:number;
        pass:number;
        cancel:number;
    }
}

export class Client extends Content implements IClient {

    account:string;
    name:string;
    mobile:string;
    address:IGoogleAddressResult;
    orderSummary:{done:number, pass:number, cancel:number};

    constructor(name?:string) {
        super();
        if (name === undefined) {
            name = "";
        }
        this.name = name;
        this.account = "";
        this.mobile = "";
        this.address = {} as IGoogleAddressResult;
        this.orderSummary = {done: 0, pass: 0, cancel: 0};
    }
}