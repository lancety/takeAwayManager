import {IClient} from "./client";
import {IProduct} from "./product";
import {IContent, Content} from "./generic";

export interface IOrder extends IContent {
    orderNumber:string;
    client:IClient;
    products:IProduct[];
    type:string;
    meal:string;
    date:Date;
    priceTotal:number;
    payment:number;
    status:string;
}

/**
 * ready  - ready for pickup or delivery
 * done   - picked up or delivered
 * queue  - waiting for get ready
 * cancel - canceled by client
 * hold   - confirmation required
 * pass   - leak of client response
 */
export const OrderStatus = {
    ready: "ready", done: "done",
    queue: "queue", cancel: "cancel",
    hold: "hold", pass: "pass"
};
export const OrderType = {
    pickUp: "pickUp", delivery: "delivery"
};
export const OrderMeal = {
    breakfast: "breakfast", lunch: "lunch",
    afternoon: "afternoon", diner: "diner"
};

export class Order extends Content implements IOrder {
    orderNumber:string;
    client:IClient;
    products:IProduct[];
    type:string;
    meal:string;
    date:Date;
    priceTotal:number;
    payment:number;
    status:string;


    constructor() {
        super();
        this.products = [];
        this.status = OrderStatus.queue;
    }
}