// field types
export const fieldType = {
    PACKAGE: "PACKAGE",
    PACKAGE_LIST: "PACKAGE_LIST",
    ADDRESS: "ADDRESS",
    ADDRESS_REF: "ADDRESS_REF",
    TEXT: "TEXT",
    TEXT_REF: "TEXT_REF",
    BTN_GROUP: "BTN_GROUP",
    TEXT_AREA: "TEXT_AREA",
    NUMBER: "NUMBER",
    DATE: "DATE",
    TIME: "TIME",
    DATE_TIME: "DATE_TIME"
};

/**
 * IPackage
 */
export interface IPackageDefinition {
    packageNameLangKey:any,
    fields:IFieldDefinition[]
}
export interface IPackageConfig {
    packageDefinition:any;
    packageLang?:any;          // either this or PackageLangKey
    packageLangKey?:string;    // root name in Lang
    packageNarrow?:boolean;
    packageMode?:string;
}
/**
 * IFieldDefinition
 */
export interface IFieldDefinition {
    key:string;            // property key of the object
    type:string;           // field type
    langKey?:string;        // language object key
    required?:boolean;
    minLength?:number;
    maxLength?:number;
    pattern?:string;
    display?:boolean;      // show on UI
    packageConfig?:IPackageConfig; // if it is a component
    visibleByGroup?:string[]    // which user group will see this field
}

/**
 * category type
 */
export interface ICategoryList {
    _id:any;
    user_id:any;
    categories:ICategoryObject[];
    updateDate?:string;
    regDate?:string;
}

export interface ICategoryObject {
    name:string;
    items:ICategoryItem[];
}

export interface ICategoryItem {
    _id:any;
    name:string;
}

export interface ICategoryRouter {
    itemNew?:string;
    itemEdit?:string;
}


/**
 * server response type
 */
// response type when 200
export const queryStatus = {
    done: "done",
    fail: "fail"
};
// response type for get, update, delete
export interface IQueryFeedback {
    status:string,
    feedback:{
        affected:number,
        affectedItems:any[]
    }
}

/**
 * model basic
 */
export interface IContent {
    _id:any;
    regDate?:string;
    updateDate?:string;
}

export class Content {
    _id:any;
    regDate:string;
    updateDate:string;

    constructor() {
        this.regDate = (new Date()).toISOString();
        this.updateDate = (new Date()).toISOString();
    }

    cloneFromSource(content:any) {
        for (let propertyKey in content) {
            if (content.hasOwnProperty(propertyKey)) {
                this[propertyKey] = content[propertyKey];
            }
        }
    }
}
