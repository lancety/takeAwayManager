
export interface IGoogleCoordination {
    lat: number;
    lng: number;
}

export interface IGoogleAddressResult {
    address_components: {
        long_name:string,
        short_name:string,
        types: string[],
    }[];
    formatted_address: string;
    geometry: {
        bounds: {
            northeast: IGoogleCoordination,
            southwest: IGoogleCoordination
        },
        location: IGoogleCoordination,
        location_type: string,
        viewport: {
            northeast: IGoogleCoordination,
            southwest: IGoogleCoordination
        }
    };
    place_id: string;
    types: string[];
}

export interface IGoogleAddressResultResponse {
    results: IGoogleAddressResult[],
    status: string;
}