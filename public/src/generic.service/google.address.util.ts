import {OptionList, OptionItem} from "../generic.component/option.list.component";
import {RequestOptionsArgs, Response} from "angular2/http";
import {Observable} from "rxjs/Observable";
import {HttpService} from "./http.service";
import {IGoogleAddressResultResponse} from "../model/google.geocoder";
export const googleGeoCoder = "http://maps.googleapis.com/maps/api/geocode/json?";


export function googleAddressResponseToOptionList (addressResults: IGoogleAddressResultResponse): OptionList{
    if (addressResults.status !== "OK") {
        return undefined;
    }

    let addressOptions = new OptionList();

    for (let result of addressResults.results){
        let addressOption = new OptionItem(result.formatted_address, result);
        addressOptions.push(addressOption);
    }

    return addressOptions
}

export function getAddressOptions(httpService: HttpService, address:string, options?: RequestOptionsArgs):Observable<any> {
    return httpService.get(googleGeoCoder + "address=" + address, options)
        .map((res: Response)=>{
            if (res.status < 200 || res.status >= 300) {
                throw new Error('Bad response status: ' + res.status);
            }
            if (res["_body"] === ""){
                return undefined;
            }else{
                if (res.json){
                    let body = res.json();
                    //return body.data || { };
                    return body || undefined;
                }else{
                    return res;
                }
            }
        })
        .catch((error: any) => {
            // In a real world app, we might send the error to remote logging infrastructure
            let errMsg = error.message || 'Server error';
            console.error(errMsg); // log to console instead
            return Observable.throw(errMsg);
        });
}