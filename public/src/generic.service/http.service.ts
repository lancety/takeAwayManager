import {Injectable} from "angular2/core";
import {Http, Response, RequestOptionsArgs} from "angular2/http";
import {Observable} from "rxjs/Rx";


@Injectable()
export class HttpService {
    constructor(private _http: Http){

    }

    get(url:string, options?: RequestOptionsArgs):Observable<any> {
        return this._http.get(url, options)
            .map((res: Response)=>{
                if (res.status < 200 || res.status >= 300) {
                    throw new Error('Bad response status: ' + res.status);
                }
                if (res["_body"] === ""){
                    return undefined;
                }else{
                    let body = res.json();
                    //return body.data || { };
                    return body || undefined;
                }
            })
            .catch((error: any) => {
                // In a real world app, we might send the error to remote logging infrastructure
                let errMsg = error.message || 'Server error';
                console.error(errMsg); // log to console instead
                return Observable.throw(errMsg);
            });
    }

    post(url:string, body:any, options?:RequestOptionsArgs):Observable<any> {
        return this._http.post(url, body, options)
            .map((res: Response)=>{
                if (res.status < 200 || res.status >= 300) {
                    throw new Error('Bad response status: ' + res.status);
                }
                if (res["_body"] === ""){
                    return undefined;
                }else{
                    let body = res.json();
                    //return body.data || { };
                    return body || undefined;
                }
            })
            .catch((error: any) => {
                // In a real world app, we might send the error to remote logging infrastructure
                let errMsg = error.message || 'Server error';
                console.error(errMsg); // log to console instead
                return Observable.throw(errMsg);
            });
    }

    put(url:string, body:any, options?:RequestOptionsArgs):Observable<any> {
        return this._http.put(url, body, options)
            .map((res: Response)=>{
                if (res.status < 200 || res.status >= 300) {
                    throw new Error('Bad response status: ' + res.status);
                }
                if (res["_body"] === ""){
                    return undefined;
                }else{
                    let body = res.json();
                    //return body.data || { };
                    return body || undefined;
                }
            })
            .catch((error: any) => {
                // In a real world app, we might send the error to remote logging infrastructure
                let errMsg = error.message || 'Server error';
                console.error(errMsg); // log to console instead
                return Observable.throw(errMsg);
            });
    }

    delete(url:string, options?:RequestOptionsArgs):Observable<any> {
        return this._http.delete(url, options)
            .map((res: Response)=>{
                if (res.status < 200 || res.status >= 300) {
                    throw new Error('Bad response status: ' + res.status);
                }
                if (res["_body"] === ""){
                    return undefined;
                }else{
                    let body = res.json();
                    //return body.data || { };
                    return body || undefined;
                }
            })
            .catch((error: any) => {
                // In a real world app, we might send the error to remote logging infrastructure
                let errMsg = error.message || 'Server error';
                console.error(errMsg); // log to console instead
                return Observable.throw(errMsg);
            });
    }
}