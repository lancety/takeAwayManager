import {IPackageDefinition, IFieldDefinition, fieldType} from "../model/generic";

export const ClientDefinition:IPackageDefinition = {
    packageNameLangKey: "client",
    fields: [
        <IFieldDefinition>{
            key: "_id",
            type: fieldType.TEXT,
            display: false
        },
        <IFieldDefinition>{
            key: "account",
            type: fieldType.TEXT
        },
        <IFieldDefinition>{
            key: "name",
            type: fieldType.TEXT,
            required: true
        },
        <IFieldDefinition>{
            key: "mobile",
            type: fieldType.TEXT
        },
        <IFieldDefinition>{
            key: "address",
            type: fieldType.ADDRESS,
        },
        <IFieldDefinition>{
            key: "regDate",
            type: fieldType.DATE_TIME
        },
        <IFieldDefinition>{
            key: "updateDate",
            type: fieldType.DATE_TIME
        }
    ]
};