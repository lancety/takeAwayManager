import {IPackageDefinition, IFieldDefinition, fieldType} from "../model/generic";

export const ProductChoiceDefinition: IPackageDefinition = {
    packageNameLangKey: "product_choice",
    fields: [
        <IFieldDefinition>{
            key: "name",
            type: fieldType.TEXT,
            required: true
        },
        <IFieldDefinition>{
            key: "priceDefault",
            type: fieldType.NUMBER,
            required: true
        },
        <IFieldDefinition>{
            key: "amountDefault",
            type: fieldType.NUMBER,
            required: true
        },
        <IFieldDefinition>{
            key: "comment",
            type: fieldType.TEXT_AREA
        }
    ]
};