
import {IPackageDefinition, IFieldDefinition, fieldType} from "../model/generic";

export const OrderClientDefinition:IPackageDefinition = {
    packageNameLangKey: "client",
    fields: [
        <IFieldDefinition>{
            key: "_id",
            type: fieldType.TEXT,
            display: false
        },
        <IFieldDefinition>{
            key: "account",
            type: fieldType.TEXT_REF
        },
        <IFieldDefinition>{
            key: "name",
            type: fieldType.TEXT_REF
        },
        <IFieldDefinition>{
            key: "mobile",
            type: fieldType.TEXT_REF
        },
        <IFieldDefinition>{
            key: "address",
            type: fieldType.ADDRESS_REF
        }
    ]
};