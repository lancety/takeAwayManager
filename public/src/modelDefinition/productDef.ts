import {IPackageDefinition, IFieldDefinition, fieldType, IPackageConfig} from "../model/generic";
import {ProductChoiceDefinition} from "./productChoiceDef";

export const ProductDefinition:IPackageDefinition = {
    packageNameLangKey: "product",
    fields: [
        <IFieldDefinition>{
            key: "_id",
            type: fieldType.TEXT,
            display: false
        },
        <IFieldDefinition>{
            key: "name",
            type: fieldType.TEXT,
            required: true
        },
        <IFieldDefinition>{
            key: "comment",
            type: fieldType.TEXT_AREA
        },
        <IFieldDefinition>{
            key: "price",
            type: fieldType.NUMBER,
            required: true
        },
        <IFieldDefinition>{
            key: "choice",
            type: fieldType.PACKAGE_LIST,
            packageConfig: <IPackageConfig>{
                packageDefinition: ProductChoiceDefinition,
                packageLangKey: "product_choice",
                packageNarrow: true
            }
        },
        <IFieldDefinition>{
            key: "regDate",
            type: fieldType.DATE_TIME
        },
        <IFieldDefinition>{
            key: "updateDate",
            type: fieldType.DATE_TIME
        }
    ]
};


let _orderProductDefinition = JSON.parse(JSON.stringify(ProductDefinition));
_orderProductDefinition.fields.push(
    <IFieldDefinition>{
        key: "amount",
        langKey: "amount",
        type: fieldType.NUMBER
    }
);
_orderProductDefinition.fields.push(
    <IFieldDefinition>{
        key: "priceTotal",
        langKey: "priceTotal",
        type: fieldType.NUMBER
    }
);

export const orderProductDefinition = _orderProductDefinition;