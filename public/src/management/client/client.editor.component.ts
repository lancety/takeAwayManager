import {Component} from "angular2/core";
import {RouteConfig, ROUTER_DIRECTIVES} from "angular2/router";
import {CategoryPackageComponent} from "../../generic.package/category.package.component";
import {PackageService} from "../../generic.package/package.editor.service";
import {CategoryService} from "../../generic.package/category.editor.service";
import {LangService} from "../../generic.lang/lang.service";
import {Client} from "../../model/client";
import {ClientDefinition} from "../../modelDefinition/clientDef";

@Component({
    selector: "client-editor",
    template: `
         <router-outlet> </router-outlet>
    `,
    directives: [ROUTER_DIRECTIVES],
    providers: [PackageService, CategoryService]
})

@RouteConfig([
    {path:"/...", name:"CategoryPackage", component:CategoryPackageComponent, useAsDefault: true}
])

export class ClientEditorComponent{

    constructor(private _packageService: PackageService) {
        // package related setting
        _packageService.packageLang = LangService.currentLanguage.client;
        _packageService.userGroup = "management";
        _packageService.packageType = "client";

        _packageService.packageDefinition = ClientDefinition;
        _packageService.getContentNew = (contentName?: string) => {
            return new Client(contentName);
        };
    }
}