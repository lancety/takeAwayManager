import {Component} from "angular2/core";
import {RouteConfig, ROUTER_DIRECTIVES} from "angular2/router";
import {CategoryPackageComponent} from "../../generic.package/category.package.component";
import {PackageService} from "../../generic.package/package.editor.service";
import {CategoryService} from "../../generic.package/category.editor.service";
import {LangService} from "../../generic.lang/lang.service";
import {Product} from "../../model/product";
import {ProductDefinition} from "../../modelDefinition/productDef";

@Component({
    selector: "product-editor",
    template: `
         <router-outlet> </router-outlet>
    `,
    directives: [ROUTER_DIRECTIVES],
    providers: [PackageService, CategoryService]
})

@RouteConfig([
    {path:"/...", name:"CategoryPackage", component:CategoryPackageComponent, useAsDefault: true}
])

export class ProductEditorComponent{

    constructor(private _packageService: PackageService) {
        // package related setting
        _packageService.packageLang = LangService.currentLanguage.product;
        _packageService.userGroup = "management";
        _packageService.packageType = "product";

        _packageService.packageDefinition = ProductDefinition;
        _packageService.getContentNew = (contentName?: string) => {
            return new Product(contentName);
        };
    }
}