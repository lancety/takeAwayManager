import {Component} from "angular2/core";
import {Router, RouteConfig, ROUTER_DIRECTIVES} from "angular2/router";

import {MenuComponent} from "./menu/menu.component";
import {LangService} from "../generic.lang/lang.service";
import {OrderNewComponent} from "./order/order.new.component";
import {OrderStatusComponent} from "./order/order.status.component";
import {OrderSummaryComponent} from "./order/order.summary.component";
import {ClientEditorComponent} from "./client/client.editor.component";
import {ProductEditorComponent} from "./product/product.editor.component";
import {HistoryListComponent} from "./history/history.list.component";
import {SummaryListComponent} from "./summary/summary.list.component";
import {HTTP_PROVIDERS} from "angular2/http";
import {HttpService} from "../generic.service/http.service";

@Component({
    selector: "take-away-manager",
    template: `
    <menu-bar>Loading...</menu-bar>
    <router-outlet></router-outlet>`,
    providers: [LangService, HttpService, HTTP_PROVIDERS],
    directives: [MenuComponent, ROUTER_DIRECTIVES]
})

@RouteConfig([
    {path:'/order_new', name: "OrderNew", component:OrderNewComponent},
    {path:'/order_status', name: "OrderStatus", component:OrderStatusComponent, useAsDefault: true},
    {path:'/order_summary', name: "OrderSummary", component:OrderSummaryComponent},
    {path:'/client_editor/...', name: "ClientEditor", component:ClientEditorComponent},
    {path:'/product_editor/...', name: "ProductEditor", component:ProductEditorComponent},
    {path:'/history_list', name: "HistoryList", component:HistoryListComponent},
    {path:'/summary_list', name: "SummaryList", component:SummaryListComponent}
])

export class Management{
    lang:string = "zh-cn";

    constructor(private _router:Router, private _langService:LangService){

    }
}