import {Component} from "angular2/core";
import {NgFor} from "angular2/common";
import {LangService, ILanguageListItem} from "../../generic.lang/lang.service";
import {IL_M_Menu} from "../../generic.lang/lang";


@Component({
    selector: "[lang]",
    template:`
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
        <span class="glyphicon glyphicon-globe"></span>
        {{langMenu.language_list}}
        <span class="caret"></span></a>
    <ul class="dropdown-menu">
        <li *ngFor="let lang of languageList; let i = index">
            <a class="lang-btn" (click)="changeLang(lang.key)">
                {{lang.title}}
            </a>
        </li>
    </ul>`,
    directives: [NgFor]
})

export class LangComponent {
    private languageList:ILanguageListItem[];
    private langMenu:IL_M_Menu;

    constructor(){
        this.languageList = LangService.languageList;
        this.langMenu = LangService.currentLanguage.management.menu;
    }

    changeLang(langKey:string){
        LangService.updateLangSetting(langKey);
    }
}