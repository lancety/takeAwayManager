import {Component, ElementRef, AfterViewInit} from "angular2/core";
import {NgIf, NgClass} from "angular2/common";
import {ROUTER_DIRECTIVES} from "angular2/router";
import {LangComponent} from "./lang.component";
import {IL_M_Menu} from "../../generic.lang/lang";
import {LangService} from "../../generic.lang/lang.service";

@Component({
    selector: "menu-bar",
    template:`
    <div class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">{{langMenu.brand}}</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li ><a id="order_new" [routerLink]="['OrderNew']"
                            class="menu-btn">{{langMenu.order_new}}</a></li>
                    <li ><a id="order_status" [routerLink]="['OrderStatus']"
                            class="menu-btn">{{langMenu.order_status}}</a></li>
                    <li ><a id="order_summary" [routerLink]="['OrderSummary']"
                            class="menu-btn">{{langMenu.order_summary}}</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li ><a id="user_editor" [routerLink]="['ClientEditor']"
                            class="menu-btn">{{langMenu.user_editor}}</a></li>
                    <li ><a id="product_editor" [routerLink]="['ProductEditor']"
                            class="menu-btn">{{langMenu.product_editor}}</a></li>
                    <li ><a id="history_list" [routerLink]="['HistoryList']"
                            class="menu-btn">{{langMenu.history_list}}</a></li>
                    <li ><a id="summary_list" [routerLink]="['SummaryList']"
                            class="menu-btn">{{langMenu.summary_list}}</a></li>
                    <li lang id="lang" class="lang dropdown"></li>
                    <li ><a href="https://gitlab.com/lancety/takeAwayManager/tree/master" target="_blank">GitLab</a></li>
                </ul>
            </div>
        </div>
    </div>`,
    directives:[NgIf, NgClass, ROUTER_DIRECTIVES, LangComponent]
})

export class MenuComponent implements AfterViewInit {
    private langMenu:IL_M_Menu;
    private element:any;

    constructor(private myElement:ElementRef){
        this.langMenu = LangService.currentLanguage.management.menu;
    }

    ngAfterViewInit(){
        var that = this;
        this.element = jQuery(this.myElement.nativeElement);
        this.element.find(".menu-btn").on("click", ()=>{
            if (that.element.find('.navbar-toggle').css('display') !== "none"){
                that.element.find('.navbar-toggle').click();
            }
        });
    }
}
