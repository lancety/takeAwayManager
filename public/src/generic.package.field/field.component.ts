import {Component, Input} from "angular2/core";
import {
    NgSwitchWhen, NgSwitchDefault, NgSwitch, NgIf, NgModel, NgFor, Control, NgControl, NgFormControl
} from "angular2/common";
import {IFieldDefinition, fieldType} from "../model/generic";
import {PackageListComponent} from "../generic.package/package.list.component";
import {FieldAddressComponent} from "./field.address.component";
@Component({
    selector: "field",
    template:`
    <div class="field-switcher"
        [ngSwitch]="fieldDefinition.type" 
        *ngIf="fieldDefinition">
        <input type="text"
            style="max-width:400px;"
            *ngSwitchDefault=""
            placeholder="{{fieldDisplayName}}"
            [(ngModel)]="packageInstance[fieldKey]" 
            [ngFormControl]="modelControl" />
        <input type="number"
            style="max-width:200px;"
            *ngSwitchWhen="fieldType.NUMBER"
            placeholder="{{fieldDisplayName}}"
            [(ngModel)]="packageInstance[fieldKey]" 
            [ngFormControl]="modelControl"/>
            
        <field-address
            style="max-width:500px;"
            *ngSwitchWhen="fieldType.ADDRESS"
            [fieldDefinition]="fieldDefinition"
            [fieldKey]="fieldKey"
            [fieldDisplayName]="fieldDisplayName"
            [(fieldModel)]="packageInstance[fieldKey]" 
            [modelControl]="modelControl"> </field-address>
            
        <textarea style="height:100px;"
            *ngSwitchWhen="fieldType.TEXT_AREA"
            placeholder="{{fieldDisplayName}}"
            [(ngModel)]="packageInstance[fieldKey]" 
            [ngFormControl]="modelControl"> </textarea>
            
        <package-list
            *ngSwitchWhen="fieldType.PACKAGE_LIST"
            [packageInstanceList]="packageInstance[fieldKey]"
            [packageConfig]="fieldDefinition.packageConfig"> </package-list>
            
        <label class="control-label control-label-datetime"
            *ngSwitchWhen="fieldType.DATE">
            {{fieldDisplayName}}{{getDateLocaleString(packageInstance[fieldKey], "date")}}</label>
        <label class="control-label control-label-datetime"
            *ngSwitchWhen="fieldType.TIME">
            {{fieldDisplayName}}{{getDateLocaleString(packageInstance[fieldKey], "time")}}</label>
        <label class="control-label control-label-datetime"
            *ngSwitchWhen="fieldType.DATE_TIME">
            {{fieldDisplayName}}{{getDateLocaleString(packageInstance[fieldKey])}}</label>
    </div>
    `,
    styles: [`
    .control-label-datetime {color:grey; font-weight: normal;}
    .field-switcher {position:relative;}
    `],
    providers: [],
    directives: [NgFor, NgIf, NgSwitch,
        NgSwitchDefault, NgSwitchWhen, NgModel, NgFormControl,
        FieldAddressComponent,
        PackageListComponent]
})

export class FieldComponent{
    @Input() fieldDefinition:IFieldDefinition;
    @Input() fieldKey: string;
    @Input() fieldDisplayName:string;
    @Input() fieldModel:any;
    @Input() packageInstance:any;
    @Input() modelControl:Control;

    private fieldType = fieldType;

    getDateLocaleString(dateTime: string, type?: string){
        switch (type){
            case "date": return (new Date(dateTime)).toLocaleDateString();
            case "time": return (new Date(dateTime)).toLocaleTimeString();
            default: return (new Date(dateTime)).toLocaleString();
        }
    }
}