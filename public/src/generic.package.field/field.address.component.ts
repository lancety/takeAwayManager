import {Component, Input, Output, EventEmitter, OnInit} from "angular2/core";
import {NgIf, Control, NgFormControl} from "angular2/common";
import {IFieldDefinition} from "../model/generic";
import {HttpService} from "../generic.service/http.service";
import * as GoogleAddressUtil from "../generic.service/google.address.util";
import {OptionList, OptionListComponent, OptionItem} from "../generic.component/option.list.component";
import {IGoogleAddressResult} from "../model/google.geocoder";


@Component({
    selector: "field-address",
    template: `
    <div class="input-group">
        <input type="text" 
        [ngModel]="addressString" 
        (ngModelChange)="getAddressOptions($event)" 
        (keydown)="navigateOption($event)"
        [ngFormControl]="modelControl"/>
        <div class="input-group-btn">
            <button class="btn btn-default btn-clean-text" (click)="cleanedOptionItem()">
                <span class="glyphicon glyphicon-minus" > </span>
            </button>
        </div>
    </div>
    <option-list
        (selectedOptionItem)="selectedOptionItem($event)"
        [optionList]="addressOptionList"> </option-list>
    `,
    directives: [NgIf, NgFormControl, OptionListComponent]
})

export class FieldAddressComponent implements OnInit{
    @Input() fieldDefinition:IFieldDefinition;
    @Input() fieldKey:string;
    @Input() fieldDisplayName:string;
    @Input() fieldModel:IGoogleAddressResult;
    @Input() modelControl:Control;
    @Input() packageInstance: any;
    @Output() fieldModelChange = new EventEmitter();

    private addressString: string;
    private addressOptionList: OptionList;

    constructor(private _httpService:HttpService){

    }

    ngOnInit(){
        this.addressString = this.fieldModel ?
            this.fieldModel.formatted_address : "";
    }

    getAddressOptions(address){
        let self = this;

        if (address) {
            GoogleAddressUtil.getAddressOptions(this._httpService, address)
                .subscribe(
                    res => {
                        self.addressOptionList =
                            GoogleAddressUtil.googleAddressResponseToOptionList(res);
                    },
                    err => {
                        console.log(err);
                    }
                )
        }else {
            self.addressOptionList = new OptionList();
        }
    }

    selectedOptionItem = (optionItem: OptionItem, $event) => {
        this.addressString = optionItem.displayName;
        this.fieldModelChange.emit(optionItem.value);
        this.addressOptionList = undefined;
        if ($event){
            $event.preventDefault();
        }
    };

    cleanedOptionItem = () => {
        this.addressString = "";
        this.fieldModelChange.emit({} as IGoogleAddressResult);
    };

    navigateOption = ($event) => {
        switch ($event.code){
            case "ArrowUp":
                this.addressOptionList.moveUp();
                break;
            case "ArrowDown":
                this.addressOptionList.moveDown();
                break;
            case "Enter":
                this.selectedOptionItem(
                    this.addressOptionList.getOption(),
                    $event);
                break;
        }
    };
}
