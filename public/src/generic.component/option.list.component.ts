import {Component, Input, Output, EventEmitter} from "angular2/core";
import {NgFor, NgIf} from "angular2/common";

export class OptionItem {
    constructor(public displayName?: string, public value?: any){
    }
}

export class OptionList {
    selectedIndex: number = 0;

    constructor(public options?: OptionItem[]){
        this.options = options || [];
    }
    
    push(option: OptionItem){
        this.options.push(option);
    }
    
    moveUp (){
        if (this.selectedIndex > 0){
            this.selectedIndex --;
        }
    }
    
    moveDown (){
        if (this.selectedIndex < this.options.length - 1){
            this.selectedIndex ++;
        }
    }

    getOption(){
        return this.options[this.selectedIndex];
    }
}

@Component({
    selector:"option-list",
    template: `
    <div *ngIf="optionList" class="list-group">
        <button type="button"  class="list-group-item" 
            [ngClass]="{active: optionList.selectedIndex === i}"
            *ngFor="let option of optionList.options; let i = index;"
            (click)="selectOptionItem(option)">{{option.displayName}}</button>
    </div>
    `,
    styles: [`
    .list-group {position:absolute; top:34px; background-color: #efefef; z-index: 1;}
    `],
    directives: [NgIf, NgFor]
})

export class OptionListComponent {
    @Input() optionList:OptionList;
    @Output() selectedOptionItem = new EventEmitter();

    selectOptionItem(option: OptionItem){
        this.selectedOptionItem.emit(option);
        this.optionList = undefined;
    }
}