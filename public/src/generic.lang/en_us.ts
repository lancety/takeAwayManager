import {ILanguageTemp} from "./lang";
export const en_us:ILanguageTemp = {
    management: {
        menu: {
            brand: "TakeAwayManager",
            order_new: "New Order",
            order_status: "Order Status",
            order_summary: "Summary",
            user_editor: "Clients",
            product_editor: "Products",
            history_list: "History",
            summary_list: "Statistic",
            language_list: "Language"
        }
    },
    client: {
        displayName: "Client",
        category_editor: {
            categoryListTitle:"Clients",
            categoryBtnAdd: "New Area",
            itemBtnAdd: "New Client"
        },
        fields: {
            _id: "ID",
            account: "Account",
            name: "Nickname",
            mobile: "Mobile",
            address: "Address",
            regDate: "Reg Date",
            updateDate: "Update Date"
        }
    },
    product: {
        displayName: "Product",
        category_editor: {
            categoryListTitle: "Category List",
            categoryBtnAdd: "New Category",
            itemBtnAdd: "New Product"
        },
        fields: {
            _id: "ID",
            name: "Name",
            comment: "Comment",
            price: "Price",
            choice: "Choice",
            amount: "Amount",
            priceTotal: "Total Price",
            regDate: "Reg Date",
            updateDate: "Update Date"
        }
    },
    product_choice: {
        displayName: "Options",
        fields: {
            name: "Title",
            priceDefault: "Default Price",
            amountDefault: "Default Amount",
            comment: "Comment"
        }
    },
    package: {
        editMode: {
            create: "Create",
            update: "Update",
            delete: "Delete"
        },
        successResponse: {
            create: " is created",
            update: " is updated",
            delete: " is deleted"
        },
        alert: {
            delete: "Deletion can not be reverted"
        }
    }
};