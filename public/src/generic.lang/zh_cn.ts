import {ILanguageTemp} from "./lang";
export const zh_cn:ILanguageTemp = {
    management: {
        menu: {
            brand: "外卖助理",
            order_new: "新订单",
            order_status: "订单列表",
            order_summary: "今日统计",
            user_editor: "客户管理",
            product_editor: "产品管理",
            history_list: "历史",
            summary_list: "统计",
            language_list: "语言"
        }
    },
    client: {
        displayName: "客户",
        category_editor: {
            categoryListTitle:"用户列表",
            categoryBtnAdd: "新区域分组",
            itemBtnAdd: "新用户"
        },
        fields: {
            _id: "ID",
            account: "账号",
            name: "昵称",
            mobile: "手机号",
            address: "地址",
            regDate: "注册日期",
            updateDate: "更新日期"
        }
    },
    product: {
        displayName: "产品",
        category_editor: {
            categoryListTitle:"产品目录",
            categoryBtnAdd: "新产品分类",
            itemBtnAdd: "新产品"
        },
        fields: {
            _id: "ID",
            name: "产品名",
            comment: "详细",
            price: "单价",
            choice: "选项",
            amount: "数量",
            priceTotal: "总价",
            regDate: "注册日期",
            updateDate: "更新日期"
        }
    },
    product_choice: {
        displayName: "产品选项",
        fields: {
            name: "选项名",
            priceDefault: "默认价格",
            amountDefault: "默认数量",
            comment: "备注"
        }
    },
    package: {
        editMode: {
            create: "新建",
            update: "更新",
            delete: "删除"
        },
        successResponse: {
            create: "已添加",
            update: "已更新",
            delete: "已删除"
        },
        alert: {
            delete: "删除操作无法撤销"
        }
    }
};