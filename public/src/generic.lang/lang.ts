export interface ILanguageTemp {
    management: IL_Management;
    client: IL_Client;
    product: IL_Product;
    product_choice: IL_Product_Choice;
    package: IL_Package;
}

/**
 * generic
 */
export interface IL_Category_Editor {
    categoryListTitle:string;
    categoryBtnAdd:string;
    itemBtnAdd:string;
}
export interface IL_Package {
    // is enum of packageEditMode
    editMode: {
        create: string,
        update: string,
        delete: string
    };
    successResponse: {
        create: string,
        update: string,
        delete: string
    };
    alert: {
        delete: string
    };
}

/**
 * management
 */
export interface IL_Management {
    menu: IL_M_Menu;
}
export interface IL_M_Menu {
    brand:string;
    order_new: string;
    order_status: string;
    order_summary: string;
    user_editor: string;
    product_editor: string;
    history_list: string;
    summary_list: string;
    language_list: string;
}

/**
 * package super
 */
export interface IL_Package_Lang {
    displayName: string;
    fields: any;
}
/**
 * client
 */
export interface IL_Client extends IL_Package_Lang {
    displayName: string;
    category_editor: IL_Category_Editor;
    fields: {
        _id: string;
        account: string;
        name: string;
        mobile: string;
        address: string;
        regDate: string;
        updateDate: string;
    };
}
/**
 * product
 */
export interface IL_Product extends IL_Package_Lang  {
    displayName: string;
    category_editor: IL_Category_Editor;
    fields: {
        _id: string;
        name: string;
        comment: string;
        price: string;
        choice: string;
        amount: string;
        priceTotal: string;
        regDate: string;
        updateDate: string;
    };
}
/**
 * product choice
 */
export interface IL_Product_Choice extends IL_Package_Lang  {
    displayName: string;
    fields: {
        name: string;
        priceDefault: string;
        amountDefault: string;
        comment: string;
    };
}