import {Injectable} from "angular2/core";

import {zh_cn} from "./zh_cn";
import {en_us} from "./en_us";
import {ILanguageTemp} from "./lang";
import {UtilService} from "../generic.service/util.service";


export const languageList:ILanguageListItem[] = [
    {
        title: "English",
        key: "en_us",
        source: en_us
    },
    {
        title: "简体中文",
        key: "zh_cn",
        source: zh_cn
    }
];

export interface ILanguageListItem {
    title: string,
    key: string,
    source: ILanguageTemp
}

@Injectable()
export class LangService {
    static currentLanguage:ILanguageTemp;
    static languageList:ILanguageListItem[] = languageList;

    constructor(){
        if (LangService.currentLanguage === undefined){
            var key = localStorage["languageDefaultKey"]|| languageList[0].key;
            LangService.currentLanguage = UtilService.copy(LangService.getLanguage(key).source);
        }
    }

    static getLanguage(key:string):ILanguageListItem{
        for(var language of LangService.languageList){
            if (language.key === key){
                return language;
            }
        }
        return undefined;
    }

    static updateLangSetting(key:string){
        //change localStorage
        localStorage.setItem("languageDefaultKey", key);
        // update value of each current language source
        UtilService.assign(LangService.currentLanguage, LangService.getLanguage(key).source);
    }
}
