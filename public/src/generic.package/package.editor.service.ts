import {Injectable} from "angular2/core";
import {RequestOptions, Headers} from "angular2/http";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import {HttpService} from "../generic.service/http.service";
import {IQueryFeedback} from "../model/generic";
import {LangService} from "../generic.lang/lang.service";
import {IL_Package} from "../generic.lang/lang";

@Injectable()
export class PackageService {
    userGroup: string;
    packageType:string;

    packageDefinition: any;
    packageLang: any;
    packageEditorLang: IL_Package;

    constructor(private _httpService:HttpService) {
        this.packageEditorLang = LangService.currentLanguage.package;
    }

    getContentNew: Function;

    getContent(contentId:string):Observable<IQueryFeedback> {
        return this._httpService.get(
            "/management/" + this.packageType + "/get_by_id/" + contentId);
    }

    createContent(content:any):Observable<IQueryFeedback> {
        let headers = new Headers();
        headers.append("content-type", "application/json;charset=UTF-8");

        let requestOptions = new RequestOptions();
        requestOptions.headers = headers;

        return this._httpService.post(
            "/management/" + this.packageType + "/create",
            JSON.stringify(content),
            requestOptions
        );
    }

    updateContent(content:any) {
        let headers = new Headers();
        headers.append("content-type", "application/json;charset=UTF-8");

        let requestOptions = new RequestOptions();
        requestOptions.headers = headers;

        return this._httpService.put(
            "/management/" + this.packageType + "/update",
            JSON.stringify(content),
            requestOptions
        );
    }

    deleteContent(content:any) {
        let headers = new Headers();
        headers.append("content-type", "application/json;charset=UTF-8");

        let requestOptions = new RequestOptions();
        requestOptions.headers = headers;
        requestOptions.body = JSON.stringify(content);

        return this._httpService.delete(
            "/management/" + this.packageType + "/delete",
            requestOptions
        );
    }
}