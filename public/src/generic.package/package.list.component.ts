import {Component, Input, OnChanges, forwardRef} from "angular2/core";
import {NgFor} from "angular2/common";
import {IPackageConfig} from "../model/generic";
import {PackageComponent} from "./package.component";
import {LangService} from "../generic.lang/lang.service";
import {ProductChoice} from "../model/productChoice";

@Component({
    selector: "package-list",
    template: `
    <div *ngFor="let packageInstance of packageInstanceList; let i = index;">
        <package class="col-xs-10 col-md-11"
            [packageInstance]="packageInstance"
            [packageConfig]="packageConfig" ></package>
        <div class="col-xs-2 col-md-1">
            <button class="btn btn-default" (click)="removeSubPackage(i)">
                <span class="glyphicon glyphicon-remove"></span>
            </button>
        </div>
        <hr/>
    </div>
    <div class="col-xs-10 col-md-11"></div>
    <div class="col-xs-2 col-md-1 ">
        <button class="btn btn-primary" (click)="createSubPackage()">
            <span class="glyphicon glyphicon-plus"></span>
        </button>
    </div>
    `,
    styles: [`
    package { padding: 10px; margin-bottom: 10px; background-color: #efefef;}
    `],
    directives: [NgFor, forwardRef(()=>PackageComponent)]
})

export class PackageListComponent implements OnChanges{
    @Input() packageConfig: IPackageConfig;
    @Input() packageInstanceList: any;

    ngOnChanges(){
        if (this.packageConfig.packageLangKey){
            this.packageConfig.packageLang = LangService.currentLanguage[this.packageConfig.packageLangKey];
        }
    }

    createSubPackage(){
        let lastPackageInstance = this.packageInstanceList[this.packageInstanceList.length - 1];
        if (!lastPackageInstance || lastPackageInstance.name){
            this.packageInstanceList.push(new ProductChoice());
        }
    }

    removeSubPackage(index: number){
        this.packageInstanceList.splice(index, 1);
    }
}