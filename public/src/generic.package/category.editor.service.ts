import {Headers, RequestOptions} from "angular2/http";
import {Injectable} from "angular2/core";
import {Observable} from "rxjs/Observable";
import {HttpService} from "../generic.service/http.service";
import {ICategoryList, ICategoryRouter, ICategoryObject, ICategoryItem, IQueryFeedback} from "../model/generic";
import {PackageService} from "./package.editor.service";

@Injectable()
export class CategoryService {
    categoryLang:any;

    list:ICategoryList;
    router:ICategoryRouter;

    userGroup:string;
    packageType:string;

    newTmpCategory:string;
    newTmpItems:any;

    syncTimer: any;

    constructor(private _httpService:HttpService,
                private _packageService: PackageService) {
        this.categoryLang = _packageService.packageLang.category_editor;
        this.router = {
            itemNew: "PackageNew",
            itemEdit: "PackageDetail"
        };
        this.initNewTmp();
    }

    initNewTmp() {
        this.newTmpCategory = "";
        this.newTmpItems = {};
    }


    getList(userGroup:string, packageType:string):Observable<ICategoryList> {
        let self = this;

        return this._httpService
            .get("/" + userGroup + "/" + packageType + "/category_list/get")
            .map((res) => {
                self.list = (res as IQueryFeedback).feedback.affectedItems[0];
                self.userGroup = userGroup;
                self.packageType = packageType;
                return res;
            });
    }

    syncList() {
        let headers = new Headers();
        headers.append("content-type", "application/json;charset=UTF-8");

        let requestOptions = new RequestOptions();
        requestOptions.headers = headers;

        let self = this;

        if (this.syncTimer){
            clearTimeout(this.syncTimer);
        }

        this.syncTimer = setTimeout(function(){
            self._httpService.post(
                "/" + self.userGroup + "/" + self.packageType + "/category_list/upsert",
                JSON.stringify(self.list),
                requestOptions).subscribe(
                res => {
                    if (!self.list._id) {
                        self.list._id = (res as IQueryFeedback).feedback.affectedItems[0]._id;
                    }
                },
                err => {
                    console.log(err);
                })
        }, 800 );
    }

    addCategory(categoryName:string) {
        if (!this.list.hasOwnProperty("categories")) {
            this.list.categories = [];
        }

        var categoryObject:ICategoryObject = {
            name: categoryName,
            items: []
        };

        this.list.categories.push(categoryObject);
        this.newTmpCategory = "";
        // update category list
        this.syncList();
    }

    removeCategory(category:ICategoryObject):boolean {
        if (!category) {
            console.log("category name is not valid");
            return;
        }

        for (let i = 0; i < this.list.categories.length; i++) {
            if (category === this.list.categories[i]) {
                for (let q=0;q < category.items.length; q++){
                    this._packageService.deleteContent(category.items[q])
                        .subscribe(
                            res => {
                                console.log(res.feedback.affectedItems[0].name +
                                            " has been removed");
                            },
                            err => {
                                console.log(err);
                            }
                        );
                }

                this.list.categories.splice(i, 1);
            }
        }
        // update category list
        this.syncList();
    }

    addItem(categoryName:string, item:any) {
        let categories = this.list.categories;

        for (let i = 0; i < categories.length; i++) {
            if (categories[i].name === categoryName) {
                categories[i].items.push({
                    _id: item["_id"],
                    name: item.name
                } as ICategoryItem);
            }
        }

        this.newTmpItems[categoryName] = "";
        // update category list
        this.syncList();
    }

    updateItem(categoryName:string, item:any) {
        let _categories = this.list.categories;
        for (let i = 0; i < _categories.length; i++) {
            let _category = _categories[i];
            if (_category.name === categoryName) {
                for (let q = 0; q < _category.items.length; q++) {
                    let _item = _category.items[q];
                    if (_item._id === item["_id"]) {
                        _item.name = item.name;
                    }
                }
            }
        }
        // update category list
        this.syncList();
    }

    removeItem(categoryName:string, item:any) {
        let _categories = this.list.categories;
        for (let i = 0; i < _categories.length; i++) {
            let _category = _categories[i];
            if (_category.name === categoryName) {
                for (let q = 0; q < _category.items.length; q++) {
                    let _item = _category.items[q];
                    if (_item._id === item["_id"]) {
                        _category.items.splice(q, 1);
                    }
                }
            }
        }
        // update category list
        this.syncList();
    }
}