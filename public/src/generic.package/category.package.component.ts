import {Component} from "angular2/core";
import {ROUTER_DIRECTIVES} from "angular2/router";
import {RouteConfig} from "angular2/router";
import {NgIf} from "angular2/common";
import {PackageEditorComponent} from "./package.editor.component";
import {PackageService} from "./package.editor.service";
import {CategoryService} from "./category.editor.service";
import {CategoryEditorComponent} from "./category.editor.component";

@Component({
    selector: "category-package",
    template:`
    <category-editor 
        class="col-xs-12 col-sm-4 col-md-3"
        *ngIf="_categoryService && _categoryService.list">
        
    </category-editor>
    <router-outlet>
    
    </router-outlet>`,
    styles: [`
    category-editor {
        margin-bottom: 30px;
    }
    `],
    directives: [NgIf, ROUTER_DIRECTIVES, CategoryEditorComponent]
})

@RouteConfig([
    {path:"/item_new", name:"PackageNew", component:PackageEditorComponent},
    {path:"/item_edit", name:"PackageDetail", component:PackageEditorComponent}
])

export class CategoryPackageComponent{
    constructor(
        private _packageService:PackageService,
        private _categoryService:CategoryService){

        this._categoryService.getList(
            this._packageService.userGroup,
            this._packageService.packageType)
            .subscribe(
                res => {},
                err => {}
            );
    }
}