import {Component, Input} from "angular2/core";
import {Router} from "angular2/router";
import {NgModel} from "angular2/common";
import {CategoryService} from "./category.editor.service";
import {ICategoryObject} from "../model/generic";
@Component({
    selector: "category-editor",
    template: `
    <h4 style="color:grey;">{{categoryLang.categoryListTitle}}</h4>
    <div class="category-list"
         *ngFor="let category of _categoryService.list.categories; let i = index;">
        <div class="category-item input-group input-group-lg">
            <input type="text" class="category-name form-control" 
                [(ngModel)]="category.name"
                (ngModelChange)="categoryNameChange($event, category.name, i)"/>
            <div class="input-group-btn">
                <button class="btn btn-default item-btn-remove category-btn" (click)="_categoryService.removeCategory(category)">
                    <span class="glyphicon glyphicon-minus" > </span>
                </button>
            </div>
        </div>
        <div class="list-item input-group input-group-lg"
             *ngFor="let item of category.items">
            <label class="item-name form-control" style="font-weight:normal;">{{item.name}}</label>
            <div class="input-group-btn">
                <button class="btn btn-default item-btn-remove "
                         (click)="openItemEditorById(category, item._id)">
                    <span class="glyphicon glyphicon-edit"> </span>
                </button>
            </div>
        </div>
        <div class="list-item list-item-new input-group input-group-lg">
            <input type="text" class="item-name form-control" [(ngModel)]="_categoryService.newTmpItems[category.name]" placeholder="{{categoryLang.itemBtnAdd}}"/>
            <div class="input-group-btn">
                <button class="btn btn-default item-btn-add"
                        (click)="openItemEditorByCategory(category)">
                    <span class="glyphicon glyphicon-plus" > </span>
                </button>
            </div>
        </div>
    </div>
    <div class="category-list">
        <div class="category-item category-item-new input-group input-group-lg">
            <input type="text" class="category-name form-control" [(ngModel)]="_categoryService.newTmpCategory" placeholder="{{categoryLang.categoryBtnAdd}}"/>  
            <div class="input-group-btn">
                <button class="btn btn-default item-btn-add category-btn"
                         (click)="_categoryService.addCategory(_categoryService.newTmpCategory)">
                    <span class="glyphicon glyphicon-plus"> </span>
                </button>
            </div>
        </div>
    </div>
    `,
    styles: [`
    .category-name::-webkit-input-placeholder {color: whitesmoke;}
    .category-name::-moz-placeholder {color: whitesmoke;}
    .category-name:-ms-input-placeholder {color: whitesmoke;}
    .category-name, .category-btn {
        background-color: #6ad;
        color: white;
    }
    .item-name {
        background-color: white;
        color: black;
    }
    .category-name, .item-name, .item-btn-add, .item-btn-remove, .item-btn-sync { 
        border:0; border-radius:0; 
    }
     
    `],
    providers: [NgModel]
})

export class CategoryEditorComponent {
    categoryLang:any;

    constructor(private _router:Router,
                private _categoryService:CategoryService) {

        this.categoryLang = _categoryService.categoryLang;
    }

    openItemEditorById(category:ICategoryObject, contentId:string) {
        this._router.navigate([
            this._categoryService.router.itemEdit,
            {
                _id: contentId,
                categoryName: category.name
            }
        ]);
    }

    openItemEditorByCategory(category:ICategoryObject) {
        this._router.navigate([
            this._categoryService.router.itemNew,
            {
                name: this._categoryService.newTmpItems[category.name],
                categoryName: category.name
            }
        ])
    }

    categoryNameChange(newValue:string, oldValue:string, index:number) {
        if (newValue){
            this._categoryService.syncList();
        }
    }
}