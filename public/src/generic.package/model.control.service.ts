import {Injectable} from "angular2/core";
import {ControlGroup} from "angular2/common";
import {ModelControlUtil} from "./model.control.util";
import {IFieldDefinition} from "../model/generic";

@Injectable()
export class ModelControlService {
    form:ControlGroup;

    constructor() {
    }

    init(fieldDefinitionList:IFieldDefinition[]) {
        this.form = ModelControlUtil.toControlGroup(fieldDefinitionList);
    }
}