
import {Injectable} from "angular2/core";
import {FormBuilder, Validators, ControlGroup, Control} from "angular2/common";
import {IFieldDefinition} from "../model/generic";
import {ValidatorFn} from "angular2/src/common/forms/directives/validators";
@Injectable()
export class ModelControlUtil {
    constructor(private fb:FormBuilder) {}
    
    static toControlGroup(fieldDefinitionList:IFieldDefinition[]){
        let group = new ControlGroup({});

        for (let field of fieldDefinitionList){
            group.addControl(field.key, ModelControlUtil.toControl(field));
        }
        
        return group;
    }

    static toControl(fieldDefinition: IFieldDefinition){
        let validatorCompose: ValidatorFn[] = [];
        if (fieldDefinition.required){
            validatorCompose.push(Validators.required)
        }
        if (fieldDefinition.minLength){
            validatorCompose.push(Validators.minLength[fieldDefinition.minLength])
        }
        if (fieldDefinition.maxLength){
            validatorCompose.push(Validators.maxLength[fieldDefinition.maxLength])
        }
        if (fieldDefinition.pattern){
            validatorCompose.push(Validators.pattern[fieldDefinition.pattern])
        }

        let control = new Control("", Validators.compose(validatorCompose));

        return control;
    }
}