import {Component, Input, OnChanges} from "angular2/core";
import {FieldComponent} from "./../generic.package.field/field.component";
import {IPackageConfig, IPackageDefinition} from "../model/generic";
import {LangService} from "../generic.lang/lang.service";
import {IL_Package} from "../generic.lang/lang";
import {FORM_DIRECTIVES, ControlGroup} from "angular2/common";
import {ModelControlService} from "./model.control.service";


@Component({
    selector: "package",
    template:`
    <form class="form-horizontal" [ngFormModel]="form"
        *ngIf="packageConfig">
        <h4 style="color:grey;" *ngIf="packageMode && packageLang && packageEditorLang">
            {{packageEditorLang.editMode[packageMode]}} {{packageLang.displayName}}
        </h4>
        <div class="form-group"  
            *ngIf="!packageConfig.packageNarrow"
            *ngFor="let field of packageDefinition.fields" >
            <label class="col-xs-12 col-sm-3 col-md-2 control-label field-label"
                *ngIf="field && field.display !== false" >
                {{packageLang.fields[field.langKey || field.key]}}</label>
            <field 
                class="col-xs-offset-1 col-xs-10 col-sm-offset-0 col-sm-8 col-md-9"
                [fieldDefinition]="field" 
                [fieldKey]="field.key"
                [packageInstance]="packageInstance"
                [modelControl]="form.controls[field.key]"
                *ngIf="field && field.display !== false" > </field>
            <div class="hide-xs col-sm-1 col-sm-offset-0 col-md-1"
                *ngIf="field && field.display !== false" ></div>
        </div>
        
        <div class="form-group field-narrow"  
            *ngIf="packageConfig.packageNarrow"
            *ngFor="let field of packageDefinition.fields" >
            <field 
                class="col-xs-12"
                [fieldDefinition]="field" 
                [fieldKey]="field.key"
                [fieldDisplayName]="packageLang.fields[field.langKey || field.key]"
                [packageInstance]="packageInstance"
                [modelControl]="form.controls[field.key]"
                *ngIf="field && field.display !== false" > </field>
        </div>
    </form>
    `,
    styles:[`
    .field-label {color:grey; }
    .form-group.field-narrow {margin-bottom: 3px; }
    `],
    directives: [FORM_DIRECTIVES, FieldComponent],
    providers: [ModelControlService]
})

export class PackageComponent implements OnChanges{
    @Input() packageInstance: any;
    @Input() packageConfig: IPackageConfig;

    private packageDefinition: IPackageDefinition;
    private packageLang: any;
    private packageMode: string;
    private packageEditorLang: IL_Package;

    private form: ControlGroup;

    constructor(private modelControlService: ModelControlService){
        this.packageEditorLang = LangService.currentLanguage.package;
    }

    ngOnChanges(changes) {
        this.packageInstance = changes.packageInstance.currentValue;

        let packageConfig = changes.packageConfig.currentValue;
        this.packageDefinition = packageConfig.packageDefinition;
        this.packageLang = packageConfig.packageLang;
        this.packageMode = packageConfig.packageMode;
        this.modelControlService.init(this.packageDefinition.fields);
        this.form = this.modelControlService.form;
    }
}
