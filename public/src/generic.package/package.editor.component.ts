import {Component, ElementRef} from "angular2/core";
import {RouteParams} from "angular2/router";
import {NgFor, NgIf} from "angular2/common";
import {IQueryFeedback, IPackageConfig} from "../model/generic";
import {CategoryService} from "./category.editor.service";
import {PackageService} from "./package.editor.service";
import {PackageComponent} from "./package.component";
import {queryStatus} from "../model/generic";
import {IL_Package} from "../generic.lang/lang";

@Component({
    selector: "package-editor",
    template: `
    <div  class="col-xs-12 col-sm-8 col-md-9 package-form-container" tabindex>
        <package-form 
            *ngIf="packageConfig.packageMode"
            [hidden]="successResponse && packageConfig.packageMode !== packageEditMode.update">
            <package
                [packageInstance]="packageInstance"
                [packageConfig]="packageConfig">
            </package>
            
            <div class="alert alert-success"
                *ngIf="successResponse">
                {{successResponse.feedback.affectedItems[0].name}}
                {{packageEditorLang.successResponse[packageConfig.packageMode]}}
            </div>
            
            <button 
                class="btn btn-primary btn-block" 
                (click)="packageCallback(packageInstance)">
                {{packageEditorLang.editMode[packageConfig.packageMode]}}</button>
            
            <div class="alert alert-danger" 
                *ngIf="packageConfig.packageMode !== packageEditMode.create">
                {{packageEditorLang.alert.delete}} 
                <button  class="btn btn-danger btn-block" 
                    (click)="deletePackage(packageInstance)">
                    {{packageEditorLang.editMode[packageEditMode.delete]}}</button>
            </div>
        </package-form>
        
        <div class="alert alert-success"
            *ngIf="successResponse && packageConfig.packageMode !== packageEditMode.update">
            {{successResponse.feedback.affectedItems[0].name}}
            {{packageEditorLang.successResponse[packageConfig.packageMode]}}
        </div>
    </div>`,
    directives: [NgFor, NgIf, PackageComponent],
    styles: [`
        .alert {
            margin-top: 20px;
        }
    `]
})

export class PackageEditorComponent {
    private contentId:string;
    private contentName:string;
    private contentCategoryName:string;

    private packageInstance:any;
    private packageConfig:IPackageConfig;

    private successResponse:IQueryFeedback;
    private packageEditorLang:IL_Package;
    private packageEditMode:any;

    constructor(private element: ElementRef,
                private _routeParams:RouteParams,
                private _packageService:PackageService,
                private _categoryService:CategoryService) {
        let self = this;

        this.packageConfig = {
            packageDefinition: _packageService.packageDefinition,
            packageLang: _packageService.packageLang
        } as IPackageConfig;

        this.packageEditorLang = _packageService.packageEditorLang;
        this.packageEditMode = packageEditMode;

        // update
        this.contentId = _routeParams.get("_id");
        // create
        this.contentName = _routeParams.get("name");
        // category name of the content
        this.contentCategoryName = _routeParams.get("categoryName");

        if (this.contentId) {
            _packageService.getContent(this.contentId)
                .subscribe(
                    res => {
                        self.packageInstance = self._packageService.getContentNew();
                        self.packageInstance.cloneFromSource(res.feedback.affectedItems[0]);
                        self.packageConfig.packageMode = packageEditMode.update;
                    },
                    err => {
                        console.log(err);
                    }
                )
        } else if (this.contentName) {
            this.packageInstance = this._packageService.getContentNew(this.contentName);
            this.packageConfig.packageMode = packageEditMode.create;
        }
    }

    packageCallback(packageInstance:any) {
        switch (this.packageConfig.packageMode) {
            case packageEditMode.update:
                this.updatePackage(packageInstance);
                break;
            case packageEditMode.create:
                this.createPackage(packageInstance);
                break;
        }
    }

    createPackage(packageInstance:any) {
        let self = this;
        this._packageService.createContent(packageInstance)
            .subscribe(
                res => {
                    if (res.status === queryStatus.done) {
                        self.successResponse = res;
                        self._categoryService.addItem(
                            self.contentCategoryName,
                            res.feedback.affectedItems[0]);
                    } else {

                    }
                },
                err => {
                    console.log(err);
                }
            );
    }

    updatePackage(packageInstance:any) {
        let self = this;
        this._packageService.updateContent(packageInstance)
            .subscribe(
                res => {
                    if (res.status === queryStatus.done) {
                        self.successResponse = res;
                        self._categoryService.updateItem(
                            self.contentCategoryName,
                            res.feedback.affectedItems[0]);
                    } else {
                    }
                },
                err => {
                    console.log(err);
                }
            );
    }

    deletePackage(packageInstance:any) {
        let self = this;
        this._packageService.deleteContent(packageInstance)
            .subscribe(
                res => {
                    if (res.status === queryStatus.done) {
                        this.packageConfig.packageMode = packageEditMode.delete;
                        self.successResponse = res;
                        self._categoryService.removeItem(
                            self.contentCategoryName,
                            res.feedback.affectedItems[0]);
                    } else {
                    }
                },
                err => {
                    console.log(err);
                }
            );
    }
}


export const packageEditMode = {
    create: "create",
    update: "update",
    delete: "delete"
};